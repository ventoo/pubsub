<?php

namespace Util\Pubsub;

interface EventSubscriber
{
    public function handleEvent(Event $event): void;

    public function subscribedTo(): string;
}
