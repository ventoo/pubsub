<?php

namespace Util\Pubsub;

interface EventPublisher
{
    public function publish(Event ...$events): void;
}
