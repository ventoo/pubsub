<?php

namespace Util\Pubsub;

use SplObjectStorage;
use function array_merge;
use function count;

class EventPublisherImpl implements EventPublisher
{
    /**
     * @var SplObjectStorage
     */
    private $subscribers;

    /**
     * @var Event[]
     */
    private $events = [];

    public function __construct(iterable $subscribers = [])
    {
        $this->subscribers = new SplObjectStorage();
        foreach ($subscribers as $subscriber) {
            $this->subscribe($subscriber);
        }
    }

    public function subscribe(EventSubscriber $subscriber): void
    {
        $this->subscribers->attach($subscriber);
    }

    public function publish(Event ...$events): void
    {
        if ($this->subscribers->count() > 0) {
            $this->events = array_merge($this->events, $events);
        }
    }

    public function dispatch(): void
    {
        if (count($this->events) > 0) {
            foreach ($this->events as $event) {
                $type = $event->type();
                /** @var EventSubscriber $subscriber */
                foreach ($this->subscribers as $subscriber) {
                    $subscribedTo = $subscriber->subscribedTo();
                    if ($subscribedTo === $type
                        || $subscribedTo === Event::class
                    ) {
                        $subscriber->handleEvent($event);
                    }
                }
            }
            $this->events = [];
        }
    }

    public function clear(): void
    {
        $this->subscribers = new SplObjectStorage();
    }
}
