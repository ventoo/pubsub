<?php

namespace Util\Pubsub;

use DateTimeImmutable;

abstract class Event
{
    public function type(): string
    {
        return static::class;
    }

    abstract public function occurredOn(): DateTimeImmutable;
}
